#1. Write a program using functions to calculate the simple interest. Suppose the
#customer is a senior citizen. He is being offered a 12 percent rate of interest, for
#all other customers, the rate of interest is 10 percent


def simple(age,r,p,t):
 print(".........................................")
 print("Age of the customer:",age)
 print("principal of the customer:",p)
 print("time period of the customer:",t)
 print("Rate of interest of the customer:",r)
 si=(r*p*t)/100
 print("The simple interest is:",si)
 print(".........................................")


age=int(input("Enter the age of customer:"))
if age>50:
 r=12
else:
 r=10
p=int(input("Enter the principal:"))
t=int(input("Enter the time peroid:"))
simple(age,r,p,t)